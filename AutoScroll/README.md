## 前言

### 支持作者

<span style="color:red;">如果你喜欢该脚本，可以打开下面的微信小程序支持一下作者。</span>  
![支持作者小程序码](https://greasyfork.org/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBekdIQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--56691abdd507118966e2810dd47b1e2a3b9b82e8/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lKYW5CbFp3WTZCa1ZVT2hSeVpYTnBlbVZmZEc5ZmJHbHRhWFJiQjJrQnlHa0J5QT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--4c3cded9533f8c872a82572269844d930809aad4/support.png?locale=zh-CN)

### 所有脚本开源地址，欢迎 star ⭐

- gitee：[https://gitee.com/Kaiter-Plus/TampermonkeyScript](https://gitee.com/Kaiter-Plus/TampermonkeyScript)
- github：[https://github.com/Kaiter-Plus/TampermonkeyScript](https://github.com/Kaiter-Plus/TampermonkeyScript)

## 1 功能介绍

给网站添加双击 T 键返回顶部的功能

### 1.1 我为什么要使用它

作为一个新时代青年，怎么可能因为移动鼠标去点击返回顶部的按钮而浪费时间呢，况且有些网站还没有返回顶部的功能按钮，小手一抬，双击 T 键它不香吗 😊

### 1.2 为什么是双击 T 呢

因为代表 ToTop（绝对不是因为懒 🤣）

## 2 更新日志

### 2.1 2022 更新日志

- 2022/3/9 给网站添加双击 T 键返回顶部的功能
