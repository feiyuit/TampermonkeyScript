## 前言

### 支持作者

<span style="color:red;">如果你喜欢该脚本，可以打开下面的微信小程序支持一下作者。</span>  
![支持作者小程序码](https://greasyfork.org/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBekdIQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--56691abdd507118966e2810dd47b1e2a3b9b82e8/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lKYW5CbFp3WTZCa1ZVT2hSeVpYTnBlbVZmZEc5ZmJHbHRhWFJiQjJrQnlHa0J5QT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--4c3cded9533f8c872a82572269844d930809aad4/support.png?locale=zh-CN)

### 所有脚本开源地址，欢迎 star ⭐

- gitee：[https://gitee.com/Kaiter-Plus/TampermonkeyScript](https://gitee.com/Kaiter-Plus/TampermonkeyScript)
- github：[https://github.com/Kaiter-Plus/TampermonkeyScript](https://github.com/Kaiter-Plus/TampermonkeyScript)

## 1 痛点分析

- 每次打开 COCO 漫画的 “我的收藏” 界面，看一下有什么漫画更新了，但是每次都要自己找最新的日期看，太麻烦了

## 2 功能介绍

- 直接给在 “我的收藏” 界面添加了按更新时间排序的功能，当然如果有多页看完请手动下一页，脚本只是在当前页面排序功能，不会访问其它页

## 3 更新记录

- 2021/03/01 COCO 漫画 我的收藏 界面根据 更新日进行排序
- 2021/03/02 保留表格标题
- 2021/03/03 最新的更新标题标为红色，更加显眼
- 2021/03/04 更新访问过的最新标题为黑色，不用自己记有没有访问过了
- 2021/10/31 COCO 漫画域名更新
