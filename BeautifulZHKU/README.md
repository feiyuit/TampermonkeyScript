## 前言

### 支持作者

<span style="color:red;">如果你喜欢该脚本，可以打开下面的微信小程序支持一下作者。</span>  
![支持作者小程序码](https://greasyfork.org/rails/active_storage/representations/redirect/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaHBBekdIQVE9PSIsImV4cCI6bnVsbCwicHVyIjoiYmxvYl9pZCJ9fQ==--56691abdd507118966e2810dd47b1e2a3b9b82e8/eyJfcmFpbHMiOnsibWVzc2FnZSI6IkJBaDdCem9MWm05eWJXRjBTU0lKYW5CbFp3WTZCa1ZVT2hSeVpYTnBlbVZmZEc5ZmJHbHRhWFJiQjJrQnlHa0J5QT09IiwiZXhwIjpudWxsLCJwdXIiOiJ2YXJpYXRpb24ifX0=--4c3cded9533f8c872a82572269844d930809aad4/support.png?locale=zh-CN)

### 所有脚本开源地址，欢迎 star ⭐

- gitee：[https://gitee.com/Kaiter-Plus/TampermonkeyScript](https://gitee.com/Kaiter-Plus/TampermonkeyScript)
- github：[https://github.com/Kaiter-Plus/TampermonkeyScript](https://github.com/Kaiter-Plus/TampermonkeyScript)

## 1 痛点分析

- 布局太丑了

## 2 主要介绍

- 由于仲恺教务网的编写和布局比较老旧，使用了很多的 <code>iframe</code> ，修改起来不容易！
- 给仲恺农业工程学院网络管理系统主页面每个功能模块添加圆角、过渡和阴影，去除一些没有用的标签！

## 3 其它介绍

- 主要是太丑看不下去，而且刚好学习<code>JavaScript</code>练练手
- 一边学<code>JavaScript</code>一边写的，所以代码写的非常乱(￣ y▽,￣)╭

## 4 功能截图
